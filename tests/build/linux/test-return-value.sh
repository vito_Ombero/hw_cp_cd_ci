#!/bin/sh

# A sh script file which captures the app's return value.

EXE=$1
ARG=$2
RETURN_CODE=1

if [ ! -f $EXE ]; then
    echo "File not found"
else
    ./$EXE $ARG
    RETURN_CODE=$?
    if [ $RETURN_CODE -eq 0 ]; then
        echo "This application has succeeded!"
    else
        echo "This application has failed!"
    fi
fi
echo "All Done."
exit $RETURN_CODE


