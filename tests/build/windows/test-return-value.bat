@echo off
 
rem A batch file which captures the app's return value.

set arg1=%1
rem using for executable

set arg2=%2
rem using for argument #1

shift
shift
rem Abling to use bat with arguments

%1 %2

@if "%ERRORLEVEL%" == "0" goto success
 
:fail
  echo This application has failed!
  echo return value = %ERRORLEVEL%
  goto end
:success
  echo This application has succeeded!
  echo return value = %ERRORLEVEL%
  goto end
:end
echo All Done
exit /B %ERRORLEVEL%

