/*
 * usefulClass.cxx
 *
 *  Created on: Sep 13, 2018
 *      Author: vito
 */

#include "../include/usefulClass.hxx"
// TODO oh god please man refactor names
namespace cpcicd {
// TODO add documentation
usefulClass::usefulClass()
    : ctorCalled(true)
    , the(new long long int[10])
{
    // TODO add exceptions
    // TODO add iterator
    for (int i = 0; i < 10; ++i) {
        this->the[i] = i;
    };
    spaceAllocated = true;
}

usefulClass::~usefulClass()
{
    // TODO switch to smart pointers
    delete[] the;
}

int usefulClass::func(int num) { return ++num; }
long long int usefulClass::getThe(int i) { return this->the[i]; }
} /* namespace cpcicd */
