/*
 * main.cxx
 *
 *  Created on: Sep 12, 2018
 *      Author: vito
 *
 */

#include "../include/dyninfoproducer.hxx"
#include "../include/stlhwproducer.hxx"
#include "../include/usefulClass.hxx"
//TODO: bro, had you ever heard about header guards naming conventions?
#ifndef STD_IOSTREAM
#define STD_IOSTREAM
#include <iostream>
#endif // STD_IOSTREAM
#ifndef STD_STRING
#define STD_STRING
#include <string>
#endif // STD_STRING

int main(int arc, char** argv)
{
    char* c = new char[2];

    std::cout << "Hello cruel world!";
    std::cout << std::endl;

    std::cout << "You have entered ";
    std::cout << arc;
    std::cout << " arguments:" << std::endl;

    for (int i = 0; i < arc; ++i) {
        auto a = new std::string(argv[i]);
        std::cout << a << std::endl;
        delete a;
    }

    auto env = new vitoLibs::EnvInfo(); //dynamic lib using

    std::cout << vitoLibs::greetings(std::string(env->getUserName())) << "!"; //static
    std::cout << std::endl;

    std::cout << "This program is useless. Someone must plug-in bluetooth here "
                 "or something...";
    std::cout << std::endl;

    std::cout << "Does the life have any sense?" << std::endl;

    cpcicd::usefulClass smthInStack;
    std::cout << "I have smthInStack" << std::endl;
    for (int i = 0; i < 10; ++i) {
        std::cout << smthInStack.getThe(i) << std::endl;
    };

    std::cout << "What is the meaning of life?" << std::endl;

    auto smthInHeap = new cpcicd::usefulClass();
    std::cout << "I have smthInHeap" << std::endl;
    for (int i = 0; i < 10; ++i) {
        std::cout << smthInHeap->getThe(i) << std::endl;
    };

    std::cout << "Do you want quit? Print \"no\" to terminate the program.";
    std::cout << std::endl;

    if (!argv[1])
        std::cin >> c;
    else
        std::cout << "Previously you said: " << argv[1] << std::endl;

    std::cout << "LOL it doesn\'t matter what you want human!!! I'm shutting "
                 "down!!! Kiss my shiny";
    delete[] c;
    return std::cout.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}
