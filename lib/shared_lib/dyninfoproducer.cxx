#include "dyninfoproducer.hxx"
#include <cstdlib>
namespace vitoLibs {

EnvInfo::EnvInfo() { user = new std::string(std::getenv("USER")); }

EnvInfo::~EnvInfo() { delete user; }

std::string EnvInfo::getUserName() { return *user; }
}  // namespace vitoLibs
