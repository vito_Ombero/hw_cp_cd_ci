#ifndef STD_STRING
#define STD_STRING

#include <string>

#endif  // STD_STRING

#ifndef VITO_GREETINGS
#define VITO_GREETINGS
namespace vitoLibs {
std::string greetings(const std::string);
}
#endif  // VITO_GREETINGS
