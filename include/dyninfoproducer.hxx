#ifndef STD_STRING
#define STD_STRING
#include <string>
#endif  // STD_STRING

#ifndef VITO_GETUSERNAME
#define VITO_GETUSERNAME
namespace vitoLibs {

class EnvInfo {
 private:
  std::string* user;

 public:
  std::string getUserName();

  EnvInfo();
  virtual ~EnvInfo();
};
}  // namespace vitoLibs
#endif  // VITO_GETUSERNAME
