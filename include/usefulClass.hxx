/*
 * usefulClass.hxx
 *
 *  Created on: Sep 13, 2018
 *      Author: vito
 */

#ifndef USEFULCLASS_HXX_
#define USEFULCLASS_HXX_

namespace cpcicd {

class usefulClass {
private:
    bool ctorCalled;
    bool spaceAllocated;
    long long int* the;

public:
    usefulClass();
    virtual ~usefulClass();

    int func(int num);
    long long int getThe(int i);
};

} /* namespace cpcicd */

#endif /* USEFULCLASS_HXX_ */
