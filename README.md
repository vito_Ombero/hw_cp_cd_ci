**What**

Cross-platfrom hello-world console application repository with CICDCD:
* CI (continues integration)
* CD (continues delivery)
* CD (continues deployment)

**Current status** : ***WIP***

## Tips for building
* Set execute permission on that file by typing chmod a+x *name*.sh and then execute the file like this: ./*name*.sh
* cmake use temp directory in **build** directory; output in **lib** and **bin**
* building for windows - you must have "cmake.exe" and "make.exe" in PATH system environmental variable for using \*.bat files in *build* directory

*still need to to do many things, e.g. - rewrite appveyor.yml to use cmake through VS to build libs*

---

## Tips for using libs

###static library

1.Create a .o file:

g++ -c stlhwproducer.hxx stlhwproducer.cxx

2.Add this file to a library:

ar rvs stlhwproducer.a stlhwproducer.o

3.use library:

g++ main.cpp stlhwproducer.a


###shared library

1. compile PIC
g++ -c -Wall -Werror -fpic dyninfoproducer.hxx dyninfoproducer.cxx

2. create a shared lib
g++ -shared -o libdyninfoproducer.so dyninfoproducer.o

3. g++ **-ldyninfoproducer** option is not looking for **dyninfoproducer.o**, but **libdyninfoproducer.so**; It main.cxx dir:
g++ -L/home/vito/eclipseCosmos/cppOpenGlws/hw_cp_cd_ci/lib/shared_lib -Wall -o program main.cxx -ldyninfoproducer

#### how to use
**LD_LIBRARY_PATH**

if 

```sh
$ echo $LD_LIBRARY_PATH
```
is empty, than

```sh
$ export LD_LIBRARY_PATH=/home/vito/eclipseCosmos/cppOpenGlws/hw_cp_cd_ci/lib/shared_lib:$LD_LIBRARY_PATH
```

**rpath**
bad way: there is locational dependency

```sh
$ unset LD_LIBRARY_PATH
$ gcc -L/home/vito/eclipseCosmos/cppOpenGlws/hw_cp_cd_ci/lib/shared_lib -Wl,-rpath=/home/vito/eclipseCosmos/cppOpenGlws/hw_cp_cd_ci/lib/shared_lib -Wall -o program main.cxx -ldyninfoproducer
```

**ldconfig** (gcc toolchain) and **ld.so**
 under root or sudo:
	
```sh
$ cp /vito/eclipseCosmos/cppOpenGlws/hw_cp_cd_ci/lib/shared_lib/libdyninfoproducer.so /usr/lib
$ chmod 0755 /usr/lib/libfoo.so
```
 update the cache

```sh
$ ldconfig
```
	
check 
```sh 
	$ ldconfig -p | grep dyninfoproducer
```
 now it's possible to 
```sh
	$ g++ -Wall -o program main.cxx -ldyninfoproducer
```
 check 
```sh
	$ ldd program | grep dyninfoproducer
```

---

## Tips for using pipelines

* use Dockerfile and follow bitbucket pipelines instructions and https://docs.docker.com/get-started/ to make your own image

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
