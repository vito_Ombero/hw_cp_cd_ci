#!/bin/sh
#set execute permission on that file by typing chmod a+x name.sh and then execute the file like this: ./create_program.sh
echo "Creating executable program..."
mkdir Debug
cd Debug
cmake ../../
make
rm -r *
echo "Done. cd ../bin "
