#!/bin/sh
#set execute permission on that file by typing chmod a+x name.sh and then execute the file like this: ./create_libs.sh
echo "Creating libs for program..."
mkdir Debug
cd Debug
cmake ../../lib/shared_lib
make
rm -r *
cmake ../../lib/static_lib
make
rm -r *
echo "Done. cd ../lib "
