
echo "Creating libs for program..."
mkdir Debug
cd Debug
cmake ../../lib/shared_lib
make
for /F "delims=" %%i in ('dir /b') do (rmdir "%%i" /s/q || del "%%i" /s/q)
cmake ../../lib/static_lib
make
for /F "delims=" %%i in ('dir /b') do (rmdir "%%i" /s/q || del "%%i" /s/q)
echo "Done. cd ../lib "
